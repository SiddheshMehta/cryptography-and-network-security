import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv("111903105_time-analysis.csv")

x = df['bit_size']
y1 = df['key_gen_time']
y2 = df['encryption_time']
y3 = df['decryption_time']

plt.plot(x, y1, label ='key genaration time')
plt.plot(x, y2, '-.', label ='encryption time')
plt.plot(x, y3, '-.+', label ='decryption time')


plt.xlabel("message size (bits)")
plt.ylabel("time taken")
plt.legend()
plt.title('RSA algorithm time analysis')
plt.show()
