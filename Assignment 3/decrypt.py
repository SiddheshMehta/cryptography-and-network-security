
def MMOD(X,Y):
	X = int(X,2)
	if X == 0:
		X = 16
	Y = int(Y,2)
	return bin((X*Y)%17)[2:].zfill(5)[1:]

def AMOD(X,Y):
	X = int(X,2)
	if X == 0:
		X = 16
	Y = int(Y,2)
	return bin((X+Y)%16)[2:].zfill(5)[1:]

def getkeymat(s):
	nibbles = [s[I:I+4] for I in range(0, len(s), 4)]
	keymat = [[]]*4
	I = 0
	J = 0
	counter = 0
	keys = []
	while I < 4:
		if counter == 6:
			keymat[I] = keys
			keys = []
			I = I+1
			counter = 0
		if counter < 6 and I < 4:
			keys.append(nibbles[J])
			counter+=1
			J+=1
		if J == 8:
			nibbles=''.join(nibbles)
			nibbles=nibbles[6:]+nibbles[:6]
			nibbles = [nibbles[K:K+4] for K in range(0, len(nibbles), 4)]
			J = 0
	lrkeymat = [nibbles[l] for l in range(J,8)][:4]
	return keymat,lrkeymat



def rounds(P,keys,round_no):
	S1 = MMOD(P[0],keys[round_no][0])
	print("S1: ", S1)
	S2 = AMOD(P[1],keys[round_no][1])
	print("S2: ", S2)
	S3 = AMOD(P[2],keys[round_no][2])
	print("S3: ", S3)
	S4 = MMOD(P[3],keys[round_no][3])
	print("S4: ", S4)
	S5 = bin(int(S1,2)^int(S3,2))[2:].zfill(5)[1:]
	print("S5: ", S5)
	S6 = bin(int(S2,2)^int(S4,2))[2:].zfill(5)[1:]
	print("S6: ", S6)
	S7 = MMOD(S5,keys[round_no][4])
	print("S7: ", S7)
	S8 = AMOD(S6,S7)
	print("S8: ", S8)
	S9 = MMOD(S8,keys[round_no][5])
	print("S9: ", S9)
	S = AMOD(S7,S9)
	print("S10: ", S)
	P[0] = bin(int(S1,2)^int(S9,2))[2:].zfill(5)[1:]
	print("S11: ", P[0])
	P[2] = bin(int(S3,2)^int(S9,2))[2:].zfill(5)[1:]
	print("S12: ", P[2])
	P[1] = bin(int(S2,2)^int(S,2))[2:].zfill(5)[1:]
	print("S13: ", P[1])
	P[3] = bin(int(S4,2)^int(S,2))[2:].zfill(5)[1:]
	print("S14: ", P[3])
	if round_no == 3:
		P[2],P[1] == P[1],P[2]
	return P


multiplicativeInverse = [0,1,9,6,13,7,3,5,15,2,12,14,10,4,11,8]
additiveInverse = [0,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1]

def decryptKeyMatrix(okeymat,lrmat):
    keys = ''.join(lrmat)
    for i in range(4,0,-1):
        keys+=''.join(okeymat[i-1][4:])+''.join(okeymat[i-1][:4])
    keys = [keys[i:i+4] for i in range(0,len(keys),4)]
    keymat = [[]]*4
    i = 0
    j = 0
    keys1 = []
    while i < 4:
        keys1.append(bin(multiplicativeInverse[int(keys[j],2)])[2:].zfill(4))
        keys1.append(bin(additiveInverse[int(keys[j+1],2)])[2:].zfill(4))
        keys1.append(bin(additiveInverse[int(keys[j+2],2)])[2:].zfill(4))
        keys1.append(bin(multiplicativeInverse[int(keys[j+3],2)])[2:].zfill(4))
        keys1.append(keys[j+4])
        keys1.append(keys[j+5])
        keymat[i] = keys1
        keys1 = []
        i = i+1
        j = j+6
    keys1.append(bin(multiplicativeInverse[int(keys[j],2)])[2:].zfill(4))
    keys1.append(bin(additiveInverse[int(keys[j+1],2)])[2:].zfill(4))
    keys1.append(bin(additiveInverse[int(keys[j+2],2)])[2:].zfill(4))
    keys1.append(bin(multiplicativeInverse[int(keys[j+3],2)])[2:].zfill(4))
    lrmat = keys1
    return keymat,lrmat




def decrypt(cipherText,key):
    if len(key)!=32:
        print("Key INVALID")
        return
    if len(cipherText)!=16:
        print("cipherText length INVALID")
        return
    keymat,lrmat = getkeymat(key)
    keymat,lrmat = decryptKeyMatrix(keymat,lrmat)
    print("\nDecryption key matrix: ")
    for i in range(0, 4):
        for j in range(0, 6):
            print(" ", keymat[i][j], end="")
        print("\n")
    Pmat = [cipherText[i:i+4] for i in range(0, len(cipherText), 4)]
    for i in range(0,4):
        Pmat = rounds(Pmat,keymat,i)
        print("Output after round "+str(i+1)+": ")
        for j in range(0,4):
            print(" ", Pmat[j], end="")
        print("\n")
    Pmat[0] = MMOD(Pmat[0],lrmat[0])
    Pmat[1] = AMOD(Pmat[1],lrmat[1])
    Pmat[2] = AMOD(Pmat[2],lrmat[2])
    Pmat[3] = MMOD(Pmat[3],lrmat[3])
    print("Final Decrypted: ")
    return ''.join(Pmat)

key = input("Enter 32 bit key: ")
cipherText = input("Enter 16 bit ciphertext: ")
print(decrypt(cipherText,key))
