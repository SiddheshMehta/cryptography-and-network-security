def MMOD(X,Y):
	X = int(X,2)
	if X == 0:
		X = 16
	Y = int(Y,2)
	return bin((X*Y)%17)[2:].zfill(5)[1:]

def AMOD(X,Y):
	X = int(X,2)
	if X == 0:
		X = 16
	Y = int(Y,2)
	return bin((X+Y)%16)[2:].zfill(5)[1:]

def getkeymat(s):
	nibbles = [s[I:I+4] for I in range(0, len(s), 4)]
	keymat = [[]]*4
	I = 0
	J = 0
	counter = 0
	keys = []
	while I < 4:
		if counter == 6:
			keymat[I] = keys
			keys = []
			I = I+1
			counter = 0
		if counter < 6 and I < 4:
			keys.append(nibbles[J])
			counter+=1
			J+=1
		if J == 8:
			nibbles=''.join(nibbles)
			nibbles=nibbles[6:]+nibbles[:6]
			nibbles = [nibbles[K:K+4] for K in range(0, len(nibbles), 4)]
			J = 0
	lrkeymat = [nibbles[l] for l in range(J,8)][:4]
	return keymat,lrkeymat



def rounds(P,keys,round_no):
	S1 = MMOD(P[0],keys[round_no][0])
	print("S1: ", S1)
	S2 = AMOD(P[1],keys[round_no][1])
	print("S2: ", S2)
	S3 = AMOD(P[2],keys[round_no][2])
	print("S3: ", S3)
	S4 = MMOD(P[3],keys[round_no][3])
	print("S4: ", S4)
	S5 = bin(int(S1,2)^int(S3,2))[2:].zfill(5)[1:]
	print("S5: ", S5)
	S6 = bin(int(S2,2)^int(S4,2))[2:].zfill(5)[1:]
	print("S6: ", S6)
	S7 = MMOD(S5,keys[round_no][4])
	print("S7: ", S7)
	S8 = AMOD(S6,S7)
	print("S8: ", S8)
	S9 = MMOD(S8,keys[round_no][5])
	print("S9: ", S9)
	S = AMOD(S7,S9)
	print("S10: ", S)
	P[0] = bin(int(S1,2)^int(S9,2))[2:].zfill(5)[1:]
	print("S11: ", P[0])
	P[2] = bin(int(S3,2)^int(S9,2))[2:].zfill(5)[1:]
	print("S12: ", P[2])
	P[1] = bin(int(S2,2)^int(S,2))[2:].zfill(5)[1:]
	print("S13: ", P[1])
	P[3] = bin(int(S4,2)^int(S,2))[2:].zfill(5)[1:]
	print("S14: ", P[3])
	if round_no == 3:
		P[2],P[1] == P[1],P[2]
	return P

def ENCRYPT(plainText,KEY):
    if len(KEY)!=32:
        print("Key INVALID")
        return
    if len(plainText)!=16:
        print("plainText length INVALID")
        return
    #get key matrix from utils file
    keymat,lrmat = getkeymat(KEY)
    print("\nEncryption key matrix: ")
    for i in range(0, 4):
        for j in range(0, 6):
            print(" ", keymat[i][j], end="")
        print("\n")
    for j in range(0, 4):
        print(" ", keymat[3][j], end="")
    print("\n")
    plainMatrix = [plainText[i:i+4] for i in range(0, len(plainText), 4)]
    for i in range(0,1):
        #rounds of addition and mult inverse using utils
        plainMatrix = rounds(plainMatrix,keymat,i)
        print("Output after round "+str(i+1) + ": ")
        for j in range(0,4):
            print(" ", plainMatrix[j], end="")
        print("\n")
    plainMatrix[0] = MMOD(plainMatrix[0],lrmat[0])
    plainMatrix[1] = AMOD(plainMatrix[1],lrmat[1])
    plainMatrix[2] = AMOD(plainMatrix[2],lrmat[2])
    plainMatrix[3] = MMOD(plainMatrix[3],lrmat[3])
    print("Final Encrypted: ")
    return ''.join(plainMatrix)

key = input("Enter 32 bit key: ")
plainText = input("Enter 16 bit plaintext: ")
cipherText = ENCRYPT(plainText,key)
print(cipherText)
