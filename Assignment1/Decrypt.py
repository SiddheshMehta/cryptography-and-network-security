import sys

I = [[1,0,0,0,0,0,0,0],
	[0,1,0,0,0,0,0,0],
	[0,0,1,0,0,0,0,0],
	[0,0,0,1,0,0,0,0],
	[0,0,0,0,1,0,0,0],
	[0,0,0,0,0,1,0,0],
	[0,0,0,0,0,0,1,0],
	[0,0,0,0,0,0,0,1]]

#text to binary
def textToBinary(plainText):
	binaryText = ""
	n = len(plainText)
	for i in range(0, n):
		binDigit = str(bin(int(ord(plainText[i]))))
		binDigit = binDigit[2:]
		binDigit = binDigit.zfill(8)
		if(len(binDigit) != 8):
			print("The text contains non-ascii characters. Example : ", plainText[i])
		binaryText = binaryText + binDigit
	if(len(binaryText) != 64):
		print("Error in code")
	return binaryText
	
def getMatrix(binaryText):
	if(len(binaryText)%8 != 0):
		print("Error")
	try:
		matrix = []
		i = 0
		while(i < len(binaryText)):
			row = []
			j = 0
			for j in range(0, 8):
				row.append(int(binaryText[i+j]))
			i = i + 8
			matrix.append(row)
		return matrix
	except:
		print("Text not in binary format")
	


def printMatrix(matrix):
	for i in range(0, 8):
		for j in range(0, 8):
			print(str(matrix[i][j]) + " " , end = "")
		print("\n")
		
def transpose(matrix):
	n = len(matrix)
	for i in range(0, n):
		for j in range(i+1, n):
			temp = matrix[i][j]
			matrix[i][j] = matrix[j][i]
			matrix[j][i] = temp
	return matrix		
	
def compliment(matrix):
	n = len(matrix)
	for i in range(0, 8):
		for j in range(0, 8):
			if(matrix[i][j] == 1):
				matrix[i][j] = 0
			else:
				matrix[i][j] = 1
	return matrix	

def XOR(A, B):
	try:
		n = len(A)
		for i in range(0, n):
			for j in range(0, n):
				if(A[i][j] == B[i][j]):
					A[i][j] = 0
				else:
					A[i][j] = 1
		return A 
	except:
		print("Error in code .")	
		
def rotate(matrix, key):
	if(key < 0):
		key = 4 - abs(key)%4
	else:
		key = key%4
	for i in range(0, key):
		matrix = transpose(matrix)
		for i in range(0, 8):
			for j in range(0, 4):
				temp = matrix[j][i]
				matrix[j][i] = matrix[8-j-1][i]
				matrix[8-j-1][i] = temp
	return matrix
		
def differentiate(matrix):
	n = len(matrix)
	for i in range(0, n):
		for j in range(0, n, 2):
			if(matrix[i][j] == 0 and matrix[i][j+1] == 1):
				continue
			elif(matrix[i][j] == 1 and matrix[i][j+1] == 1):
				matrix[i][j] = 0
				matrix[i][j+1] = 0
			elif(matrix[i][j] == 0 and matrix[i][j+1] == 0):
				matrix[i][j] = 1
				matrix[i][j+1] = 0
			elif(matrix[i][j] == 1 and matrix[i][j+1] == 0):
				matrix[i][j] = 1
				matrix[i][j+1] = 1
	return matrix
	
def integrate(matrix):
	n = len(matrix)
	for i in range(0, n):
		for j in range(0, n, 2):
			if(matrix[i][j] == 0 and matrix[i][j+1] == 1):
				continue
			elif(matrix[i][j] == 1 and matrix[i][j+1] == 1):
				matrix[i][j] = 1
				matrix[i][j+1] = 0
			elif(matrix[i][j] == 0 and matrix[i][j+1] == 0):
				matrix[i][j] = 1
				matrix[i][j+1] = 1
			elif(matrix[i][j] == 1 and matrix[i][j+1] == 0):
				matrix[i][j] = 0
				matrix[i][j+1] = 0
	return matrix
	

def matrixToText(matrix):
	text = ""
	for i in range(0, 8):
		binString = ""
		for j in range(0, 8):
			binString = binString + str(matrix[i][j])
		num = 0
		x = 0
		j = 7
		while(j >= 0):
			num = num + int(binString[j])*(2**x)
			x = x + 1
			j = j - 1
		letter = chr(num)
		text = text + letter
	return text


def adjustText(plainText):
	n = len(plainText)
	k = 8 - n%8
	if(k == 8):
		return plainText
	for i in range(0, k):
		plainText = plainText + " " 
	return plainText




def makeSegments(plainText):
	textSegments = []
	n = len(plainText)
	lowerLimit = 0
	higherLimit = 8
	while(higherLimit <= n+1):
		segment = plainText[lowerLimit : higherLimit]
		textSegments.append(segment)
		lowerLimit = higherLimit
		higherLimit = higherLimit + 8
	return textSegments
	
f = open(sys.argv[1], 'r')
cypherText = f.read()
	
textSegments = makeSegments(cypherText)
matrixList = []	
for segment in textSegments:
	matrixList.append(getMatrix(textToBinary(segment)))	
n = len(matrixList)
text = ""
for i in range(0, n):
	matrix = matrixList[i]
	matrix = transpose(matrix)
	matrix = integrate(matrix)
	matrix = rotate(matrix, -2)
	matrix = transpose(matrix)
	matrix = compliment(matrix)
	matrix = XOR(matrix, I)
	matrix = transpose(matrix)
	text = text + matrixToText(matrix)

try:
	f = open(sys.argv[2], 'w')
	f.write(text)
except:
	f = open("DECRYPTED.txt", 'w')
	f.write(text)
