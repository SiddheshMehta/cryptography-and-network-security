f = open("plaintext.txt", "r")
text = f.read()

def encrypt(text, key):
	n = len(text)
	op = ""
	for i in range(0, n):
		try:
			op = op + chr((ord(text[i]) + key)%256)
		except:
			print(ord(text[i]), text[i])
	return op
	
cipherText = encrypt(text, 3)
print("Encrypted text : \n", cipherText) 

f = open("ciphertext.txt", "w")
f.write(cipherText)
f.close()
