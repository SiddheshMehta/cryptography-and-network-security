f = open("plaintext.txt", "r")
text = f.read()

def encrypt(text, key):
	n = len(text)
	op = ""
	for i in range(0, n):
		try:
			op = op + chr((ord(text[i]) + key)%256)
		except:
			print(ord(text[i]), text[i])
	return op
	
def decrypt(text, key):
	n = len(text)
	op = ""
	for i in range(0, n):
		num = (ord(text[i]) - key)%256
		if(num < 0):
			num = num + 256
		op = op + chr(num)		
	return op
	
cipherText = encrypt(text, 3)
print("Encrypted text : \n", cipherText) 
plainText = decrypt(cipherText, 3)
print("Decrypted text : \n", plainText) 

