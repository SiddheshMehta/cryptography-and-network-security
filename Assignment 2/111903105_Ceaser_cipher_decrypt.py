f = open("ciphertext.txt", "r")
cipherText = f.read()


def decrypt(text, key):
	n = len(text)
	op = ""
	for i in range(0, n):
		num = (ord(text[i]) - key)%256
		if(num < 0):
			num = num + 256
		op = op + chr(num)		
	return op
	
	
plainText = decrypt(cipherText, 3)
print("Decrypted text : \n", plainText) 
