f = open("plaintext.txt", "r")
text = f.read()

def encrypt(plainText, a, b):
	
	cipherText = ""
	n = len(plainText)
	for i in range(0, n):
		cipherText = cipherText + chr((a*ord(plainText[i]) + b)%256)
	return cipherText
	
def decrypt(cipherText, a, b):
	
	a_inv = modInverse(a, 256)
	plainText = ""
	n = len(cipherText)
	for i in range(0, n):
		num = (a_inv*( ord(cipherText[i]) - b ))%256
		if(num < 0):
			num = num + 256
		plainText = plainText + chr(num)
	return plainText
	


x, y = 0, 1
 
# Function for extended Euclidean Algorithm
 
 
def gcdExtended(a, b):
    global x, y
 
    # Base Case
    if (a == 0):
        x = 0
        y = 1
        return b
 
    # To store results of recursive call
    gcd = gcdExtended(b % a, a)
    x1 = x
    y1 = y
 
    # Update x and y using results of recursive
    # call
    x = y1 - (b // a) * x1
    y = x1
 
    return gcd
 
 
def modInverse(A, M):
 
    g = gcdExtended(A, M)
    if (g != 1):
        print("Inverse doesn't exist")
 
    else:
 
        # m is added to handle negative x
        res = (x % M + M) % M
        return res
 
cipherText = encrypt(text, 3, 11)
print("Encrypted text : \n", cipherText) 
plainText = decrypt(cipherText, 3,11)
print("Decrypted text : \n", plainText) 

